using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health;
    public GameObject enemy;

    public void TakeDamage (float hp)
    {
        health -= hp;

        if (health <= 0f)
            Destroy(enemy);
    }
}
