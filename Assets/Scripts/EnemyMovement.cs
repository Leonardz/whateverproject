using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class EnemyMovement : MonoBehaviour
{
    public float forwardSpeed;
    public CinemachineDollyCart dolly;

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Player")
        {
            SetSpeed(forwardSpeed); ;
        }
    }

    void SetSpeed(float x)
    {
        dolly.m_Speed = x;
    }
}
