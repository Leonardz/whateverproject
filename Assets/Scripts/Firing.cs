using UnityEngine;

public class Firing : MonoBehaviour
{
    public float fireRange;
    public float damage = 10f;
    public GameObject Scatter;  

    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
            Shoot();
    }

    void Shoot()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, fireRange))
        {
            Debug.Log(hit.transform.name);

            Enemy target = hit.transform.GetComponent<Enemy>();
            if (target != null && target.tag == "Enemy")
            {
                GameObject ImpactGo = Instantiate(Scatter, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(ImpactGo, 1f);
                target.TakeDamage(damage);               
            }
        }
    }
}
