using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallOnTrue : MonoBehaviour
{
    public bool falling = false;
    public GameObject pico;
    private Rigidbody rb;

    void Start()
    {
        rb = pico.GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Player")
        {
            rb.isKinematic = false;
        }
    }
}
