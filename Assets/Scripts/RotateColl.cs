using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateColl : MonoBehaviour
{
    public GameObject gameCamera;
    public GameObject playerObject;

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Player")
        {
            playerObject.GetComponent<PlayerMovement>().enabled = false;
            playerObject.GetComponent<RotatePlayerMovement>().enabled = true;
            gameCamera.transform.Rotate(0, -90, 0);
            Destroy(GetComponent<BoxCollider>());
        }
    }
}
