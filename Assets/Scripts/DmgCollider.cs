using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmgCollider : MonoBehaviour
{
    public GameObject playerObject;
    PlayerHealth p_Health;
    public int attackDamage = 20;

    void Awake()
    {
        p_Health = playerObject.GetComponent<PlayerHealth>();
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Player")
        {
            Debug.Log("Trap Hit");
            p_Health.TakeDamage(attackDamage);
            Destroy(GetComponent<BoxCollider>());
        }
        else
        {
            Debug.Log("Trap not Hit");
        }
    }
}
