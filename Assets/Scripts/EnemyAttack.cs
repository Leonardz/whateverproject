using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public float fireRange;
    public GameObject attackingObject;
    public Transform playerTarget;
    public GameObject playerObject;
    public float fireRate;
    private float nextFire;
    PlayerHealth p_Health;
    public int attackDamage = 1;
    private GameObject objectThatLooks;
    private GameObject objectToLook;
    private Vector3 objectToLookPosition;

    void Awake()
    {
        p_Health = playerObject.GetComponent<PlayerHealth>();
    }

    private void Start()
    {
        objectToLook = GameObject.FindGameObjectWithTag ("Player");
    }

    void Update()
    {
        {
            transform.LookAt(playerTarget);
        }

        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;

            RaycastHit hits;
            if (Physics.Raycast(attackingObject.transform.position, transform.TransformDirection(Vector3.forward), out hits, fireRange))
            {
                Debug.DrawRay(attackingObject.transform.position, transform.TransformDirection(Vector3.forward) * hits.distance, Color.yellow);
                Debug.Log("Did Hit");
                p_Health.TakeDamage(attackDamage);
            }
            else
            {
                Debug.DrawRay(attackingObject.transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                Debug.Log("Did not Hit");
            }
        }
    }

}
