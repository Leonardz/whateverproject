using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public GameObject player;
    public int startingHealth = 100;
    public int currentHealth;
    public Image damageImage;
    public float flashSpeed;
    public Color flashColor = new Color(1f, 0f, 0f, 0.1f);

    bool damaged = false;

    void Awake()
    {
        currentHealth = startingHealth;
    }

    void Update()
    {
        if(damaged == true)
        {
            damageImage.color = flashColor;
            if (currentHealth == 0)
            {
                Destroy(player);
            }
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        damaged = false;
    }

    public void TakeDamage(int amount)
    {
        currentHealth = currentHealth - amount;
        damaged = true;
    }
}
