using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTrap : MonoBehaviour
{
    public float k;

    void Update()
    {
        transform.Rotate(new Vector3(0, 0, Time.deltaTime * k));
    }
}
